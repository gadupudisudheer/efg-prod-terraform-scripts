resource "aws_efs_file_system" "efg-PR" {
	creation_token = "EFG-EFS-PR"
	performance_mode = "generalPurpose"
	throughput_mode = "provisioned"
	encrypted = "true"
	provisioned_throughput_in_mibps = 200
	
	lifecycle_policy {
		transition_to_ia = "AFTER_30_DAYS"
	}
	
     tags   = {
        "ApplicationAcronym"   = "EFG"
        "ApplicationCI"        = "EFG"
        "ApplicationName"      = "Enterprise File Gateway"
        "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
        "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
        "Backup"               = "False"
        "BusinessOwner"        = "Bpost"
        "BusinessUnit"         = "Middleware"
        "CloudServiceProvider" = "AWS"
        "CompanyName"          = "Infosys"
        "CostCentre"           = "Bpost"
        "DRLevel"              = "1"
        "DataProfile"          = "Internal"
        "Environment"          = "PROD"
        "ManagedBy"            = "Infosys"
        "Name"                 = "efs-sharedapp-pr-euwe01-hdd-001"
        "Owner"                = "Ramesh"
        "PatchGroup"           = "RHEL"
        "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
        "Project"              = "EFG"
        "ResourceType"         = "EFS FileShare"
        "Tier"                 = "App"
     }
}

resource "aws_efs_mount_target" "EFG-mount-target" {
	count = 3
	file_system_id  = aws_efs_file_system.efg-PR.id
	subnet_id       = element(var.EFS_subnet_id, count.index)
	security_groups = ["sg-063c34f22d2f180b6"]
	
}
	