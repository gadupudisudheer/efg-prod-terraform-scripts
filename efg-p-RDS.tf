data "aws_subnet_ids" "all" {
	vpc_id = var.vpc_id
}

resource "aws_db_subnet_group" "rds-subnet" {
	name  		= "rsn-sharedapp-pr-euwe01-efg-01"
	subnet_ids  = var.db_subnet_group_ids
	description = "subnet group"
	tags        = {
		"ApplicationAcronym" = "EFG"
    }
}
 
resource "aws_db_instance" "EFG-PR-DB" {
	allocated_storage  				    = 570
	max_allocated_storage               = 1000
	storage_type 						= "io1"
	iops								= "4000"
	engine 		 						= "oracle-se2"
	engine_version 						= "12.2.0.1.ru-2020-01.rur-2020-01.r1"
	instance_class						= "db.m5.2xlarge"
	db_subnet_group_name				= aws_db_subnet_group.rds-subnet.id
	storage_encrypted					= "true"
	license_model						= "license-included"
	name								= "EFGPRDB"
	username							= "admin"
	#password							= "password2020"
	performance_insights_enabled        = true
	port 								= "1523"
	iam_database_authentication_enabled = "false"
	vpc_security_group_ids				= ["sg-01eaa4ee3da21ea5e"]
	maintenance_window					= "sun:01:30-sun:02:00"
	backup_window						= "00:30-01:00"
	backup_retention_period				= 14
	#final_snapshot_identifier			= "EFG-PR-Final-Snapshot"
	skip_final_snapshot                 = true
	character_set_name					= "AL32UTF8"
	deletion_protection					= "true"
	auto_minor_version_upgrade			= "false"
	copy_tags_to_snapshot				= "true"
	multi_az							= "true"
	kms_key_id							= "arn:aws:kms:eu-west-1:788299903371:key/8f13f4ec-d873-4aca-9e27-13248643c72c"
	
	enabled_cloudwatch_logs_exports       = [
      "alert",
      "audit",
      "listener",
      "trace"
    ]
	monitoring_interval                   = 60
	
tags	= {
    "ApplicationAcronym"   = "EFG"
    "ApplicationCI"        = "EFG"
    "ApplicationName"      = "Enterprise File Gateway"
    "ApplicationOwner"     = "Ramesh.Balachandran.ext@bpost.be"
    "ApplicationSupport"   = "Ramesh.Balachandran.ext@bpost.be"
    "AvailabilityZone"     = "eu-west-1a"
    "Backup"               = "False"
    "BusinessOwner"        = "Bpost"
    "BusinessUnit"         = "Middleware"
    "CloudServiceProvider" = "AWS"
    "CompanyName"          = "Infosys"
    "CostCentre"           = "Bpost"
    "DRLevel"              = "1"
    "DataProfile"          = "Internal"
    "Environment"          = "PROD"
    "ManagedBy"            = "Infosys"
    "Name"                 = "rdi-sharedapp-pr-euwe01-efg-02"
    "Owner"                = "Ramesh"
    "PatchGroup"           = "Oracle"
    "PrimaryContact"       = "Ramesh.Balachandran.ext@bpost.be"
    "Project"              = "EFG"
    "ResourceType"         = "RDS"
    "Tier"                 = "App"
    }
}

resource "aws_sns_topic" "default" {
	name 	= "EFG-PR-DB-Events"
tags 		= {
	
	Name = "sns-sharedapp-pr-euwe01-efg-01"
	ApplicationAcronym = "EFG"
	ApplicationCI = "EFG"
	ApplicationOwner = "Ramesh.Balachandran.ext@bpost.be"
	ApplicationSupport = "Ramesh.Balachandran.ext@bpost.be"
	Backup = "False"
	BusinessOwner = "Bpost"
	BusinessUnit = "Middleware"
	CloudServiceProvider = "AWS"
	CompanyName = "Infosys"
	CostCentre = "Bpost"
	DataProfile = "Internal"
	DRLevel = "1"
	Environment = "PROD"
	Owner = "Ramesh"
	PatchGroup = "Oracle"
	Project = "EFG"
	ResourceType = "SNS"
	Tier = "App"
	}	
}

resource "aws_db_event_subscription" "default" {
	name 		= "EFG-PR-DB-Events-Subs"
	sns_topic   = "arn:aws:sns:eu-west-1:788299903371:sns-sharedapp-pr-euwe01-efg-topic-01"
				  #aws_sns_topic.default.arn
	source_type = "db-instance"
	source_ids  = []#[aws_db_instance.EFG-PR-DB.id]
	
	event_categories = [
		"availability",
		"deletion",
		"failover",
		"low storage",
		"maintenance",
		"notification",
		"read replica",
		"recovery",
		"restoration",
	]

     tags   = {
        "ApplicationAcronym" = "EFG"
        "Name"               = "res-sharedapp-pr-euwe01-efg-01"
     }
}

