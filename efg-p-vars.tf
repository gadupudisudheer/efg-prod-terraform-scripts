variable "region"{
	default = "eu-west-1"
}

variable "vpc_cidr" {
	default = "10.76.160.0/19"	
}

variable "vpc_id" {
	default = "vpc-00ac32c28ae93cf9d"
}	

variable "subnet_cidr" {
		default = "10.76.166.0/23"
}

variable "public_subnet_cidr" {
		default = "10.76.162.0/24"
}

# eu-west-1c || euw1-az1
variable "subnet_id" {
	default = "subnet-0d4113d49d12617d4"
}

variable "EFS_subnet_id" {
type = list
default = ["subnet-0d4113d49d12617d4","subnet-01f2464ee3ab08ae3","subnet-004b8ce7f0d385084"]
}

variable "public_subnet_id" {
	default = "subnet-0d33af2bd8f103812"#"subnet-0e824c6ceca22d471"
}

variable "db_subnet_group_ids" {
	type = list
	default = ["subnet-0aca11c6bea38a12f", "subnet-0465344e5e4870ab4", "subnet-0038642191a44a662"]
}


variable "azs" {
type = list
 	default = ["eu-west-1c"]
}

variable "ec2-ami" {
  default = "ami-08dc6987db6e0b656"
   
}

variable "alarms_email" {
 	default = "ramesh.balachandran.ext@bpost.be"
}

variable "instance-type"{
	default = "c5n.xlarge"
}

variable "core-instance-type"{
	default = "r5n.2xlarge"
}


variable "ec2-count"{
	default = "1"
}

variable "kms_key" {
     default = "arn:aws:kms:eu-west-1:788299903371:key/f60eea71-859a-45f9-a794-7d08d2180d9e"
}

#data "aws_availability_zones" "azs" {}'