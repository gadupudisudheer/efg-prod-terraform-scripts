provider "aws" {
	
 region = "eu-west-1"
 profile = "terraform"
 assume_role{
	role_arn = "arn:aws:iam::788299903371:role/iar-MigrationTeam"
 }

}